# 👾 Ejecución del servidor

## 🐱‍🚀 Requisitos previos

-   Asegúrate de tener instalado nodemon de manera global, si no lo tienes, lo puedes instalar corriendo con el comando `npm install -g nodemon` o `yarn global add nodemon` según sea tu caso.
-   Asegúrate de ejecutar el script de la base de datos en PostgreSQL.

## 👩‍🚀 Pasos para la configuracion

1. Clonar el repositorio y dirigirse a la carpeta generada.
2. Dirigirse a la rama solicitudes con `git checkout solicitudes`.
3. Instalar las dependencias necesarias con `npm install` o `yarn` según sea tu caso.
4. En la raíz debes crear 2 archivos `.env` y `.env-cmdrc` en los cuales debes poner:
    - En él `.env`:
    ```
        DATABASE_URL="postgresql://<usuario>:<contraseña>@localhost:5432/<base_de_datos>?schema=<public o tu esquema>"
        # Ejemplo: DATABASE_URL="postgresql://chavo:sinnombre@localhost:5432/arboles?schema=public"
    ```
    - En él `.env-cmdrc`:
    ```json
    {
        {
           "dev": {
                "PORT": <puerto>,
                "SECRET_TOKEN": "<token>"
            }
        }
    }
    ```
    - El puerto puede ser el que elijas como `3000`, `4000`, `4040`, yo uso el `5050` tú puedes poner el mismo o elegir otro, pero eso sí, debe ser un número no un string (o sea que no debe estar entre comillas)
    - El token puede ser cualquier string, solo asegúrate que sea seguro con símbolos como $,%, mayúsculas, números, etc.
5. Una vez hayas creado todos los archivos corres el siguiente comando `npx prisma generate` para que puedas ocupar el esquema que prisma ya genero automáticamente.
6. Cuando ya esté todo configurado puedes simplemente correr el comando `npm run dev` o `yarn dev` según sea tu caso.

# 🦉 Advertencia

1. En caso de hacer algún cambio notificarme para hacer pull 😈
2. Si haces algún cambio y quieres hacer push debes hacer push a la rama con `git push origin solicitudes` si lo hace de otra forma todo se irá al caño.
