const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const { getTokenData } = require("../encrypt/tokenManager");

async function isAuthenticated(req, res, next) {
    try {
        const authorization = req.headers.authorization;
        const data = getTokenData(authorization);
        if (!data) return res.status(401).json({ message: "No autorizado" });
        const { user_id: id_usuario } = data;
        const user = await prisma.usuario.findUnique({ where: { id_usuario } });
        if (!user) return res.status(401).json({ message: "No autorizado" });
        req.user = user;
        next();
    } catch {
        return res.status(401).json({ message: "No autorizado" });
    }
}

module.exports = isAuthenticated;
