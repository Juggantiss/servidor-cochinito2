const express = require("express");
const morgan = require("morgan");
const ApolloServer = require("./graphql");

const app = express();

const PORT = process.env.PORT || 3000;

app.use(morgan("common"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("src/public"));

// Express routes
const userRoute = require("./routes/User.routes");
const acountRoute = require("./routes/Account.routes");
const moveRoute = require("./routes/Move.routes");

app.use("/users", userRoute);
app.use("/accounts", acountRoute);
app.use("/moves", moveRoute);

// GraphQL configuration
ApolloServer.applyMiddleware({ app });

app.listen(PORT, () => {
    console.log(`Server running on http://localhost:${PORT}/`);
});
