const moment = require("moment-timezone");

const setNumberFormat = (time) => {
    return time > 9 ? String(time) : "0" + String(time);
};

const utils = {
    now() {
        return moment.tz(Date.now(), "America/Mexico_City").format();
    },
    parseTime(date) {
        const dateFormatted = new Date(date);
        const hours = setNumberFormat(dateFormatted.getUTCHours());
        const minutes = setNumberFormat(dateFormatted.getUTCMinutes());
        return `${hours}:${minutes}`;
    },
    getDateToString() {
        const dateFormatted = new Date(this.now());
        const day = setNumberFormat(dateFormatted.getDate());
        const month = setNumberFormat(dateFormatted.getMonth() + 1);
        const year = dateFormatted.getFullYear();
        return `${day}/${month}/${year}`;
    },
};

module.exports = utils;
