const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getMoveType = async (move_id) => {
    try {
        if (!move_id) return false;
        const move = await prisma.tipo_movimiento.findUnique({
            where: {
                id_tipomov: move_id,
            },
        });
        if (!move) return false;

        switch (move.tipo) {
            case "deposito":
                return "increment";
            case "retiro":
                return "decrement";
            default:
                return false;
        }
    } catch {
        return false;
    }
};

module.exports = getMoveType;
