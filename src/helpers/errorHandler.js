const errorHandler = (error) => {
    console.log(error);
    let errorMessage = "No se pudo realizar la operación";
    const parseError = JSON.parse(JSON.stringify(error));
    if (parseError) {
        if (!parseError?.code) return { message: errorMessage };
        switch (parseError.code) {
            case "P2002":
                errorMessage = `Ya existe un campo con el mismo ${parseError.meta.target[0]}.`;
                break;
            default:
                return { message: errorMessage };
        }
    }
    return { message: errorMessage };
};

module.exports = errorHandler;
