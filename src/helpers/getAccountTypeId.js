const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getAccountTypeId = async (tipo) => {
    try {
        const tipoDeCuenta = await prisma.tipo_cuenta.findFirst({
            where: {
                tipo,
            },
            select: {
                id_tipocuenta: true,
            },
        });

        if (tipoDeCuenta) {
            return tipoDeCuenta.id_tipocuenta;
        }

        const newAccountType = await prisma.tipo_cuenta.create({
            data: {
                tipo,
            },
        });
        return newAccountType.id_tipocuenta;
    } catch {
        return null;
    }
};

module.exports = getAccountTypeId;
