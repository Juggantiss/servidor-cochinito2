const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const searchUserAccount = async (user_id, account_id) => {
    if (!user_id || !account_id) return false;
    const cuenta = await prisma.r_tiene.findFirst({
        where: {
            AND: [{ clv_cuenta: account_id }, { id_usuario: user_id }],
        },
        select: {
            cuenta: true,
        },
    });
    if (!cuenta) return false;
    return cuenta.cuenta;
};

module.exports = searchUserAccount;
