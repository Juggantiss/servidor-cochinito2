const bcrypt = require("bcrypt");

const hashPassword = async (passwrod) => {
    return await bcrypt.hash(passwrod, await bcrypt.genSalt(10));
};

module.exports = hashPassword;
