const { sign, verify } = require("jsonwebtoken");

module.exports = {
    generateToken(user_id) {
        return sign({ user_id }, process.env.SECRET_TOKEN);
    },
    getTokenData(authorization) {
        try {
            const token = authorization.replace("Bearer ", "");
            const data = verify(token, process.env.SECRET_TOKEN);
            return data;
        } catch {
            return null;
        }
    },
};
