const { GraphQLScalarType } = require("graphql");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const { parseTime } = require("../../utils");

const TimeScalar = new GraphQLScalarType({
    name: "Time",
    description: "Le da formato a las horas que trae postgres",
    serialize(value) {
        return parseTime(value);
    },
});

const types = {
    Time: TimeScalar,
    Persona: {
        async nombreCompleto(parent) {
            try {
                const { nombre, app, apm } = parent;
                return `${nombre} ${app} ${apm}`;
            } catch (error) {
                throw new Error(error);
            }
        },
    },
    Usuario: {
        async datosPersonales({ id_persona }) {
            try {
                const persona = await prisma.persona.findUnique({
                    where: { id_persona },
                });
                return persona;
            } catch (error) {
                throw new Error(error);
            }
        },
        async cuentas(parent, args, { user }) {
            try {
                const cuentas = await prisma.r_tiene.findMany({
                    where: {
                        AND: [
                            {
                                usuario: {
                                    id_usuario: user.id_usuario,
                                },
                            },
                            {
                                cuenta: {
                                    favorito: false,
                                },
                            },
                        ],
                    },
                    select: {
                        cuenta: {
                            select: {
                                clv_cuenta: true,
                                saldo: true,
                                descripcion: true,
                                fecha_creacion: true,
                                favorito: true,
                                id_tipocuenta: true,
                            },
                        },
                    },
                    orderBy: {
                        cuenta: {
                            fecha_creacion: "asc",
                        },
                    },
                });

                const arregloDeCuentas = cuentas.map((cuenta) => cuenta.cuenta);

                return arregloDeCuentas;
            } catch (error) {
                throw new Error(error);
            }
        },
        async cuentasFavoritas(parent, args, { user }) {
            try {
                const cuentas = await prisma.r_tiene.findMany({
                    where: {
                        AND: [
                            {
                                usuario: {
                                    id_usuario: user.id_usuario,
                                },
                            },
                            {
                                cuenta: {
                                    favorito: true,
                                },
                            },
                        ],
                    },
                    select: {
                        cuenta: {
                            select: {
                                clv_cuenta: true,
                                saldo: true,
                                descripcion: true,
                                fecha_creacion: true,
                                favorito: true,
                                id_tipocuenta: true,
                            },
                        },
                    },
                    orderBy: {
                        cuenta: {
                            fecha_creacion: "desc",
                        },
                    },
                });

                const arregloDeCuentas = cuentas.map((cuenta) => cuenta.cuenta);

                return arregloDeCuentas;
            } catch (error) {
                throw new Error(error);
            }
        },
        async solicitudesPendientes(parent, args, { user }) {
            try {
                const { id_usuario } = user;

                const solicitudes = await prisma.solicitudes_enviadas.findMany({
                    where: {
                        AND: [
                            {
                                usuario: {
                                    id_usuario,
                                },
                            },
                            {
                                estado: false,
                            },
                        ],
                    },
                    select: {
                        solicitud: true,
                    },
                });

                const arregloDeSolicitudes = solicitudes.map(
                    ({ solicitud }) => solicitud
                );

                return arregloDeSolicitudes;
            } catch (error) {
                throw new Error(error);
            }
        },
    },
    Cuenta: {
        async tipo(parent) {
            try {
                const tipoDeCuenta = await prisma.tipo_cuenta.findUnique({
                    where: {
                        id_tipocuenta: parent.id_tipocuenta,
                    },
                });
                return tipoDeCuenta;
            } catch (error) {
                throw new Error(error);
            }
        },
        async usuarios(parent) {
            try {
                const cuentas = await prisma.r_tiene.findMany({
                    where: { clv_cuenta: parent.clv_cuenta },
                    select: {
                        usuario: true,
                    },
                });
                const usuariosDeLaCuenta = cuentas.map((cuenta) => cuenta.usuario);
                return usuariosDeLaCuenta;
            } catch (error) {
                throw new Error(error);
            }
        },
        async movimientos(parent) {
            try {
                const movimientos = await prisma.movimiento.findMany({
                    where: { clv_cuenta: parent.clv_cuenta },
                    orderBy: [{ fecha_movimiento: "desc" }, { hora: "desc" }],
                });

                return movimientos;
            } catch (error) {
                throw new Error(error);
            }
        },
    },
    Solicitud: {
        async enviadoPor({ id_solicitante }, args, context) {
            try {
                const solicitante = await prisma.usuario.findUnique({
                    where: {
                        id_usuario: id_solicitante,
                    },
                });

                return solicitante;
            } catch (error) {
                throw new Error(error);
            }
        },
        async cuenta({ clv_cuenta }) {
            try {
                const cuenta = await prisma.cuenta.findUnique({
                    where: { clv_cuenta },
                });

                return cuenta;
            } catch (error) {
                throw new Error(error);
            }
        },
    },
};

module.exports = types;
