const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const errorHandler = require("../../helpers/errorHandler");
const searchUserAccount = require("../../helpers/searchUserAccount");

const queries = {
    _: () => true,
    async aboutMe(_, __, { user }) {
        try {
            if (!user) return null;
            return user;
        } catch (error) {
            throw new Error(error);
        }
    },
    async allAcountTypes() {
        try {
            const accountTypes = await prisma.tipo_cuenta.findMany({
                orderBy: { id_tipocuenta: "asc" },
            });
            return accountTypes;
        } catch (error) {
            throw new Error(error);
        }
    },
    async allMoveTypes() {
        try {
            const moveTypes = await prisma.tipo_movimiento.findMany({
                orderBy: { id_tipomov: "asc" },
            });
            return moveTypes;
        } catch (error) {
            throw new Error(error);
        }
    },
    async getAccount(parent, { id }, { user }) {
        try {
            if (!user) return null;
            const cuenta = await searchUserAccount(user.id_usuario, id);
            if (!cuenta) return null;
            return cuenta;
        } catch (error) {
            errorHandler(error);
        }
    },
};

module.exports = queries;
