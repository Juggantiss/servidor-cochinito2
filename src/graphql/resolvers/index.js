const mutations = require("./mutations");
const queries = require("./queries");
const types = require("./types");

module.exports = {
    Mutation: mutations,
    Query: queries,
    ...types,
};
