const { ApolloServer } = require("apollo-server-express");
const path = require("path");
const fs = require("fs");

const typeDefsPath = path.join(__dirname, "typeDefs.graphql");
const typeDefs = fs.readFileSync(typeDefsPath, "utf-8");
const resolvers = require("./resolvers");
const context = require("./context");
let playground = process.env.NODE_ENV === "development";

const server = new ApolloServer({
    typeDefs,
    resolvers,
    context,
    playground,
});

module.exports = server;
