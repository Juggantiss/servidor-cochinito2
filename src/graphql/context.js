const { getTokenData } = require("../encrypt/tokenManager");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

async function context({ req }) {
    const { headers } = req;
    const token = headers?.authorization;

    if (!token) return {};
    const { user_id: id_usuario } = getTokenData(token);

    if (!id_usuario) return {};
    const user = await prisma.usuario.findUnique({ where: { id_usuario } });

    return { user };
}

module.exports = context;
