const { Router } = require("express");

const router = Router();

const isAuthenticated = require("../auth/isAuthenticated");
const verifyAccount = require("../middlewares/verifyAccount");
const UserController = require("../controllers/User.controller");

router.post("/registro", UserController.createUser);
router.post("/login", UserController.login);

router.post(
    "/send-request",
    isAuthenticated,
    verifyAccount,
    UserController.sendRequest
);
router.post("/accept-request/:id", isAuthenticated, UserController.acceptRequest);

module.exports = router;
