const { Router } = require("express");
const isAuthenticated = require("../auth/isAuthenticated");

const router = Router();

const AccountController = require("../controllers/Account.controlller");

router.post("/type", isAuthenticated, AccountController.types.newType);
router.route("/type/:id").put(isAuthenticated, AccountController.types.updateType);

router.post("/", isAuthenticated, AccountController.createAccount);

router.post(
    "/favorite/:id",
    isAuthenticated,
    AccountController.changeFavoriteStatus
);

module.exports = router;
