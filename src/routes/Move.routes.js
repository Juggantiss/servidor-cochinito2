const { Router } = require("express");

const router = Router();

const MoveController = require("../controllers/Move.controller");

// Middlewares
const isAuthenticated = require("../auth/isAuthenticated");
const verifyAccount = require("../middlewares/verifyAccount");

router.post("/type", isAuthenticated, MoveController.types.newType);
router.route("/type/:id").put(isAuthenticated, MoveController.types.updateType);

router.post("/", isAuthenticated, verifyAccount, MoveController.createMove);

module.exports = router;
