const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const searchUserAccount = require("../helpers/searchUserAccount");

const verifyAccount = async (req, res, next) => {
    const id_usuario = req.user.id_usuario;
    const clv_cuenta = req.body?.clv_cuenta;
    const account = await searchUserAccount(id_usuario, clv_cuenta);
    if (!account) return res.json({ message: "No se pudo realizar la operación" });
    req.account = account;
    next();
};

module.exports = verifyAccount;
