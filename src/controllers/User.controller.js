const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const hashPassword = require("../encrypt/hashPassword");
const verifyPassword = require("../encrypt/verifyPassword");
const { generateToken } = require("../encrypt/tokenManager");
const errorHandler = require("../helpers/errorHandler");
const getAccountTypeId = require("../helpers/getAccountTypeId");
const utils = require("../utils");

module.exports = {
    async createUser(req, res) {
        try {
            const { email, pass, usuario, descripcion, ...personData } = req.body;
            const hashedPassword = await hashPassword(pass);

            const personalAccountTypeId = await getAccountTypeId("personal");
            await prisma.persona.create({
                data: {
                    ...personData,
                    usuario: {
                        create: {
                            email,
                            password: hashedPassword,
                            usuario,
                            cuentas: {
                                create: {
                                    cuenta: {
                                        create: {
                                            descripcion: "Cuenta personal",
                                            tipo_cuenta: {
                                                connect: {
                                                    id_tipocuenta: personalAccountTypeId,
                                                },
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            });

            res.json({
                message: "Usuario creado correctamente",
                userName: usuario,
            });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
    async login(req, res) {
        try {
            const userExists = await prisma.usuario.findFirst({
                where: {
                    OR: [{ email: req.body?.email }, { usuario: req.body?.usuario }],
                },
            });

            if (userExists) {
                const passwordMatch = await verifyPassword(
                    req.body?.password,
                    userExists.password
                );

                if (passwordMatch) {
                    const token = generateToken(userExists.id_usuario);
                    res.json({ token, tokenGeneratedAt: utils.now() });
                } else {
                    res.json({ message: "Su información es incorrecta" });
                }
            } else {
                res.json({ message: "Su información es incorrecta" });
            }
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
    async sendRequest(req, res) {
        try {
            const { id_usuario } = req.user;
            const { clv_cuenta } = req.account;
            const { motivo, receptores } = req.body;

            const solicitud = await prisma.solicitudes.create({
                data: {
                    motivo,
                    usuario: {
                        connect: {
                            id_usuario,
                        },
                    },
                    cuenta: {
                        connect: {
                            clv_cuenta,
                        },
                    },
                },
            });

            receptores.forEach(async (receptor) => {
                await prisma.solicitudes_enviadas.create({
                    data: {
                        id_solicitud: solicitud.id,
                        id_receptor: receptor,
                    },
                });
            });

            res.json({ message: "Operación exitosa" });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
    async acceptRequest(req, res) {
        try {
            const id_usuario = req.user.id_usuario;
            const id = req.params.id;

            const today = utils.getDateToString();

            const solicitud = await prisma.solicitudes_enviadas.findFirst({
                where: {
                    AND: [
                        {
                            id_receptor: id_usuario,
                        },
                        {
                            id_solicitud: id,
                        },
                    ],
                },
                include: {
                    solicitud: {
                        select: {
                            clv_cuenta: true,
                        },
                    },
                },
            });

            if (!solicitud)
                return res.json({
                    message: "No cuenta con una solicitud con estos párametros",
                });

            await prisma.solicitudes_enviadas.update({
                where: {
                    id: solicitud.id,
                },
                data: {
                    estado: true,
                    fecha_aceptacion: today,
                },
            });

            const cuentaCompartidaId = await getAccountTypeId("compartida");

            await prisma.cuenta.update({
                where: {
                    clv_cuenta: solicitud.solicitud.clv_cuenta,
                },
                data: {
                    tipo_cuenta: {
                        connect: {
                            id_tipocuenta: cuentaCompartidaId,
                        },
                    },
                },
            });

            await prisma.r_tiene.create({
                data: {
                    usuario: {
                        connect: {
                            id_usuario,
                        },
                    },
                    cuenta: {
                        connect: {
                            clv_cuenta: solicitud.solicitud.clv_cuenta,
                        },
                    },
                },
            });

            res.json({ message: "Operación exitosa" });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
};
