const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const errorHandler = require("../helpers/errorHandler");
const getMoveType = require("../helpers/getMoveType");

module.exports = {
    types: {
        async newType(req, res) {
            try {
                const tipo = req.body?.type;
                if (!tipo) return res.json({ message: "El tipo es requerido" });
                await prisma.tipo_movimiento.create({
                    data: {
                        tipo,
                    },
                });
                res.json({
                    message: "Tipo de movimiento creado correctamente",
                    typeName: tipo,
                });
            } catch (error) {
                return res.json(errorHandler(error));
            }
        },
        async updateType(req, res) {
            try {
                const id_tipomov = parseInt(req.params?.id);
                const tipo = req.body?.type;
                if (!tipo) return res.json({ message: "El tipo es requerido" });
                const type = await prisma.tipo_movimiento.update({
                    where: {
                        id_tipomov,
                    },
                    data: {
                        tipo,
                    },
                });
                res.json({
                    message: "Tipo de movimiento actualizado correctamente",
                    type,
                });
            } catch (error) {
                return res.json(errorHandler(error));
            }
        },
    },
    async createMove(req, res) {
        try {
            const { cantidad, motivo, id_tipomov } = req.body;
            const { id_usuario } = req.user;
            const { clv_cuenta, saldo: saldo_anterior } = req.account;

            // Saber el tipo de movimiento
            const operationType = await getMoveType(id_tipomov);
            if (!operationType)
                return res.json({ message: "Operación no disponible" });

            if (operationType === "decrement" && cantidad > saldo_anterior) {
                return res.json({ message: "No cuenta con los fondos suficientes" });
            }

            await prisma.cuenta.update({
                where: {
                    clv_cuenta,
                },
                data: {
                    saldo: {
                        [operationType]: cantidad,
                    },
                    movimientos: {
                        create: {
                            cantidad,
                            saldo_anterior,
                            motivo,
                            usuario: {
                                connect: {
                                    id_usuario,
                                },
                            },
                            tipo_movimiento: {
                                connect: {
                                    id_tipomov,
                                },
                            },
                        },
                    },
                },
            });

            res.json({ message: "Operación exitosa" });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
};
