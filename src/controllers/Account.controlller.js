const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const errorHandler = require("../helpers/errorHandler");

module.exports = {
    types: {
        async newType(req, res) {
            try {
                const tipo = req.body?.type;
                if (!tipo) return res.json({ message: "El tipo es requerido" });
                await prisma.tipo_cuenta.create({
                    data: {
                        tipo,
                    },
                });
                res.json({
                    message: "Tipo de cuenta creado correctamente",
                    typeName: tipo,
                });
            } catch (error) {
                return res.json(errorHandler(error));
            }
        },
        async updateType(req, res) {
            try {
                const id_tipocuenta = parseInt(req.params?.id);
                const tipo = req.body?.type;
                if (!tipo) return res.json({ message: "El tipo es requerido" });
                const type = await prisma.tipo_cuenta.update({
                    where: {
                        id_tipocuenta,
                    },
                    data: {
                        tipo,
                    },
                });
                res.json({
                    message: "Tipo de cuenta actualizado correctamente",
                    type,
                });
            } catch (error) {
                return res.json(errorHandler(error));
            }
        },
    },
    personalAccount: {},
    sharedAccount: {},
    async createAccount(req, res) {
        try {
            const { descripcion, id_tipocuenta } = req.body;

            const cuenta = await prisma.cuenta.create({
                data: {
                    descripcion,
                    id_tipocuenta,
                    usuarios: {
                        create: {
                            id_usuario: req.user.id_usuario,
                        },
                    },
                },
            });

            res.json({
                message: "Cuenta creada correctamente",
                accountCreated: cuenta,
            });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
    async changeFavoriteStatus(req, res) {
        try {
            const clv_cuenta = req.params?.id;
            if (!clv_cuenta)
                return res.json({ message: "No se pudo realizar la operación" });
            // Busca una cuenta para obtener el estado de favorito
            const cuenta = await prisma.cuenta.findUnique({
                where: {
                    clv_cuenta,
                },
                select: {
                    favorito: true,
                },
            });
            if (!cuenta)
                return res.json({ message: "No se pudo realizar la operación" });
            // Asigna el estado contrario a la cuenta (si está en true pasa false y si esta false para true)
            const cuentaActualizada = await prisma.cuenta.update({
                where: { clv_cuenta },
                data: { favorito: !cuenta.favorito },
            });
            res.json({ message: "Operación exitosa", accout: cuentaActualizada });
        } catch (error) {
            return res.json(errorHandler(error));
        }
    },
};
